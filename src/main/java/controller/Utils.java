package controller;

import io.socket.client.IO;

import java.util.Objects;

/**
 * User: jacek
 * Date: 4/8/20
 * Time: 2:28 PM
 * <p/>
 * Copyright 2020 Ambrosoft, Inc. All Rights Reserved.
 * This software is the proprietary information of Ambrosoft, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
final class Utils {
    // make lexicographically smaller the controlling peer
    static boolean isControlling(final String myId, final String remoteId) {
        final int compare = Objects.requireNonNull(myId).compareTo(Objects.requireNonNull(remoteId));
        if (compare == 0) {
            throw new IllegalArgumentException("to and from cannot be the same");
        } else {
            return compare < 0;
        }
    }

    static IO.Options createOptions() {
        final IO.Options opts = new IO.Options();
        opts.forceNew = true;
        return opts;
    }
}
