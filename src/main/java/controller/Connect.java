package controller;

import io.socket.client.IO;
import io.socket.client.Socket;
import org.ice4j.ice.*;
import org.ice4j.pseudotcp.PseudoTcpSocket;
import org.ice4j.pseudotcp.PseudoTcpSocketFactory;
import org.json.JSONObject;
import test.SdpUtils;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * User: jacek
 * Date: 4/8/20
 * Time: 2:19 PM
 * <p/>
 * Copyright 2020 Ambrosoft, Inc. All Rights Reserved.
 * This software is the proprietary information of Ambrosoft, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
final class Connect {
    private static final Logger logger = Logger.getLogger(Connect.class.getName());
    /**
     * Local job thread variable
     */
    private static ReaderPseudoTcpJob readerPseudoTcpJob;
    /**
     * Remote job thread variable
     */
    private static SenderPseudoTcpJob senderPseudoTcpJob;
    /**
     * Test data size
     */
    private static final int TEST_BYTES_COUNT = 2_000_000;
    private static final boolean USE_STUN = true;
    private static final boolean USE_TURN = true;
    /**
     * Monitor object used to wait for local agent to finish it's job
     */
    private static final Object localAgentMonitor = new Object();
    /**
     * Timeout for ICE discovery
     */
    private static long agentJobTimeout = 15000;
    static final String SIGNALLING_URI = "http://fast-bastion-38385.herokuapp.com/chat";
    //     static   final String uri = "http://localhost:5000/chat";

    public static void main(String[] args) throws Throwable {
        final String myEmail = args[0];
        final String remoteEmail = args[1];
        final boolean isControlling = Utils.isControlling(myEmail, remoteEmail);

        System.setProperty("org.ice4j.ipv6.DISABLED", "true");

        int portNumber = 6543;

        final Agent agent = IcePseudoTcp.createAgent(portNumber);
        agent.setControlling(isControlling);
        if (isControlling) {
            agent.setNominationStrategy(NominationStrategy.NOMINATE_HIGHEST_PRIO);
            agent.addStateChangeListener(new LocalIceProcessingListener());
        } else {
            agent.addStateChangeListener(new RemoteIceProcessingListener());
        }

        // this is just a synchronization device
        final BlockingQueue<Object> values = new LinkedBlockingQueue<>();
        final Socket socket = IO.socket(SIGNALLING_URI, Utils.createOptions());
        socket.on(Socket.EVENT_CONNECT, objects -> {
            try {
                final JSONObject intro = new JSONObject();
                intro.put("type", "P2P");
                intro.put("fromId", myEmail);
                intro.put("toId", remoteEmail);
                socket.emit("intro", intro);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).on("user connected", objs -> {
            System.out.println("user connected = " + Arrays.toString(objs));
            try {
                final String localSDP = SdpUtils.createSDPDescription(agent);
//                System.out.println("localSDP = " + localSDP);
//                System.out.println("sending offer ...");
                socket.emit("offer", myEmail, remoteEmail, localSDP);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }).on("offer forward", objs -> {
//            System.out.println("offer forward = " + Arrays.toString(objs));
//            System.out.println("answering ...");
            try {
                final String remoteSdp = (String) objs[2];
//                System.out.println(remoteSdp);
                controller.SdpUtils.parseSDP(agent, remoteSdp);
                final String localSDP = SdpUtils.createSDPDescription(agent);
//                System.out.println("localSDP = " + localSDP);
                socket.emit("answer", myEmail, remoteEmail, localSDP);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
            values.offer("done");
        }).on("answer forward", objs -> {
            System.out.println("answer forward = " + Arrays.toString(objs));
//                            socket.emit("answer", myEmail, remoteEmail, "answer by " + myEmail);
            try {
                final String remoteSdp = (String) objs[2];
                System.out.println(remoteSdp);
                controller.SdpUtils.parseSDP(agent, remoteSdp);
            } catch (Exception e) {
                e.printStackTrace();
            }

            values.offer("done");
        }).on("response", objs -> System.out.println("response args = " + Arrays.toString(objs)))
                .on("message", objs -> System.out.println("message args = " + Arrays.toString(objs)));

        socket.connect();
        // the below call will block until handshake above is completed and peers have exchanged SDPs
        values.take();
        System.out.println("after take; starting connectivity");

        agent.startConnectivityEstablishment();

        socket.off();
        socket.disconnect();
        socket.close();
    }

    private static class ReaderPseudoTcpJob extends Thread implements Runnable {
        private final DatagramSocket datagramSocket;

        ReaderPseudoTcpJob(final DatagramSocket socket) {
            this.datagramSocket = socket;
        }

        @Override
        public void run() {
            logger.log(Level.INFO, "Reader PSEUDO worker started");
            try {
                logger.log(Level.INFO, "Reader PSEUDOTCP is using: " + datagramSocket.getLocalSocketAddress() + datagramSocket);

                final PseudoTcpSocket socket = new PseudoTcpSocketFactory().createSocket(datagramSocket);
                socket.setConversationID(0b0100_0000_0000_0000_0000_0000_0000_0000);
                socket.setMTU(1500);
                socket.setDebugName("L");
                logger.log(Level.INFO, "before accept");

                socket.accept(9000);
                logger.log(Level.INFO, "after accept");


                final byte[] buffer = new byte[TEST_BYTES_COUNT];
                int read = 0;
                while (read < TEST_BYTES_COUNT) {
                    read += socket.getInputStream().read(buffer, read, buffer.length - read);
                    logger.log(Level.INFO, "Local job read: " + read);
                }

                logger.log(Level.INFO, "now writing");

                socket.getOutputStream().write(buffer);
                socket.getOutputStream().flush();

                logger.log(Level.INFO, "finished writing after reading (Reader)");


                //TODO: close when all received data is acked
//                socket.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            logger.log(Level.INFO, "Local pseudotcp worker finished");
        }
    }

    private static class SenderPseudoTcpJob extends Thread implements Runnable {
        private final DatagramSocket datagramSocket;
        private final InetSocketAddress peerAddr;

        SenderPseudoTcpJob(DatagramSocket socket, InetSocketAddress peerAddr) {
            this.datagramSocket = socket;
            this.peerAddr = peerAddr;
        }

        @Override
        public void run() {
            logger.log(Level.INFO, "Sender PSEUDO worker started");
            try {
                logger.log(Level.INFO, "Sender PSEUDOTCP is using: " + datagramSocket.getLocalSocketAddress() + " and will communicate with: " + peerAddr);

                final PseudoTcpSocket socket = new PseudoTcpSocketFactory().createSocket(datagramSocket);
                socket.setConversationID(0b0100_0000_0000_0000_0000_0000_0000_0000);
                socket.setMTU(1500);
                socket.setDebugName("R");
                final long start = System.currentTimeMillis();
                socket.connect(peerAddr, 9000);
                
                final byte[] buffer1 = new byte[TEST_BYTES_COUNT];
                {
                    final ThreadLocalRandom random = ThreadLocalRandom.current();
                    for (int i = buffer1.length; --i >= 0; ) {
                        buffer1[i] = (byte) random.nextInt(256);
                    }
                }
                socket.getOutputStream().write(buffer1);
                socket.getOutputStream().flush();
                //Socket will be closed by the iceAgent
//                socket.close();
                final long end = System.currentTimeMillis();
                logger.log(Level.INFO, "Transferred " + TEST_BYTES_COUNT + " bytes in " + ((end - start) / 1000) + " sec");

                logger.log(Level.INFO, "now reading back");

                final byte[] buffer2 = new byte[TEST_BYTES_COUNT];
                int read = 0;
                while (read < TEST_BYTES_COUNT) {
                    read += socket.getInputStream().read(buffer2, read, buffer2.length - read);
                    logger.log(Level.INFO, "Local job read: " + read);
                }
                logger.log(Level.INFO, "end of reading after writing (Sender)");

                boolean same = Arrays.equals(buffer1, buffer2);
                System.out.println("same = " + same);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            logger.log(Level.INFO, "Remote pseudotcp worker finished");
        }
    }

    static final class LocalIceProcessingListener implements PropertyChangeListener {
        /**
         * System.exit()s as soon as ICE processing enters a final state.
         *
         * @param evt the {@link PropertyChangeEvent} containing the old and new
         *            states of ICE processing.
         */
        public void propertyChange(final PropertyChangeEvent evt) {
            final Object iceProcessingState = evt.getNewValue();
            logger.log(Level.INFO, "Local agent entered the " + iceProcessingState + " state.");
            if (iceProcessingState == IceProcessingState.COMPLETED) {
                final Agent agent = (Agent) evt.getSource();
                logger.log(Level.INFO, "Local: Create pseudo tcp stream");
                final IceMediaStream dataStream = agent.getStream("data");
                final Component udpComponent = dataStream.getComponents().get(0);
                final CandidatePair selectedPair = udpComponent.getSelectedPair();
                if (selectedPair != null) {
                    final LocalCandidate localCandidate = selectedPair.getLocalCandidate();
                    final Candidate<?> remoteCandidate = selectedPair.getRemoteCandidate();
                    logger.log(Level.INFO, "Local: " + localCandidate);
                    logger.log(Level.INFO, "Remote: " + remoteCandidate);
                    readerPseudoTcpJob = new ReaderPseudoTcpJob(selectedPair.getDatagramSocket());
                } else {
                    logger.log(Level.INFO, "Failed to select any candidate pair");
                }
            } else if (iceProcessingState == IceProcessingState.TERMINATED) {
                /*
                 * Though the process will be instructed to die, demonstrate
                 * that Agent instances are to be explicitly prepared for
                 * garbage collection.
                 */
                if (readerPseudoTcpJob != null) {
                    readerPseudoTcpJob.start();
                }
                synchronized (localAgentMonitor) {
                    localAgentMonitor.notifyAll();
                }
            } else if (iceProcessingState == IceProcessingState.FAILED) {
                synchronized (localAgentMonitor) {
                    localAgentMonitor.notifyAll();
                }
            }
        }
    }

    static final class RemoteIceProcessingListener implements PropertyChangeListener {
        /**
         * System.exit()s as soon as ICE processing enters a final state.
         *
         * @param evt the {@link PropertyChangeEvent} containing the old and new
         *            states of ICE processing.
         */
        public void propertyChange(final PropertyChangeEvent evt) {
            final Object iceProcessingState = evt.getNewValue();
            logger.log(Level.INFO, "Remote agent entered the " + iceProcessingState + " state.");
            if (iceProcessingState == IceProcessingState.COMPLETED) {
                final Agent agent = (Agent) evt.getSource();
                logger.log(Level.INFO, "Remote: Create pseudo tcp stream");
                final IceMediaStream dataStream = agent.getStream("data");
                final Component udpComponent = dataStream.getComponents().get(0);
                final CandidatePair selectedPair = udpComponent.getSelectedPair();
                if (selectedPair != null) {
                    final LocalCandidate localCandidate = selectedPair.getLocalCandidate();
                    final Candidate<?> remoteCandidate = selectedPair.getRemoteCandidate();
                    logger.log(Level.INFO, "Remote: Local address " + localCandidate);
                    logger.log(Level.INFO, "Remote: Peer address " + remoteCandidate);
                    senderPseudoTcpJob = new SenderPseudoTcpJob(selectedPair.getDatagramSocket(), remoteCandidate.getTransportAddress());
                } else {
                    logger.log(Level.SEVERE, "Remote: Failed to select any candidate pair");
                }
            } else if (iceProcessingState == IceProcessingState.TERMINATED) {
                if (senderPseudoTcpJob != null) {
                    senderPseudoTcpJob.start();
                }
                synchronized (localAgentMonitor) {
                    localAgentMonitor.notifyAll();
                }
            } else if (iceProcessingState == IceProcessingState.FAILED) {
                synchronized (localAgentMonitor) {
                    localAgentMonitor.notifyAll();
                }
            }
        }
    }
}
