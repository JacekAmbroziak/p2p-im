package controller;

import java.io.IOException;
import java.net.Socket;

/**
 * User: jacek
 * Date: 4/11/20
 * Time: 3:11 PM
 * <p/>
 * Copyright 2020 Ambrosoft, Inc. All Rights Reserved.
 * This software is the proprietary information of Ambrosoft, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
interface Connectable {
    void connected(final Socket socket, final boolean isControlling) throws IOException;

    void exception(final IOException e);

    void p2pFailed();
}
