package controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * User: jacek
 * Date: 4/11/20
 * Time: 4:50 PM
 * <p/>
 * Copyright 2020 Ambrosoft, Inc. All Rights Reserved.
 * This software is the proprietary information of Ambrosoft, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
final class PeerB {
    private static final Logger logger = Logger.getLogger(PeerB.class.getName());
    private static final int TEST_BYTES_COUNT = 2_000_000;

    public static void main(String[] args) {
        try {
            final P2PConnector connector = new P2PConnector("B", "A");
            connector.connect(new B());
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    static class B implements Connectable {
        @Override
        public void connected(final Socket socket, final boolean isControlling) throws IOException {
            logger.log(Level.INFO, "object B connected()");
            final InputStream inputStream = socket.getInputStream();
            final OutputStream outputStream = socket.getOutputStream();
            if (isControlling) {
                final byte[] buffer1 = new byte[TEST_BYTES_COUNT];
                {
                    final ThreadLocalRandom random = ThreadLocalRandom.current();
                    for (int i = buffer1.length; --i >= 0; ) {
                        buffer1[i] = (byte) random.nextInt(256);
                    }
                }
                final long start = System.currentTimeMillis();

                outputStream.write(buffer1);
                outputStream.flush();
                //Socket will be closed by the iceAgent
//                socket.close();
                final long end = System.currentTimeMillis();
                logger.log(Level.INFO, "Transferred " + TEST_BYTES_COUNT + " bytes in " + ((end - start) / 1000) + " sec");

                logger.log(Level.INFO, "now reading back");
                final byte[] buffer2 = new byte[TEST_BYTES_COUNT];
                int read = 0;
                while (read < TEST_BYTES_COUNT) {
                    read += inputStream.read(buffer2, read, buffer2.length - read);
//                    logger.log(Level.INFO, "Local job read: " + read);
                }
                logger.log(Level.INFO, "end of reading after writing (Sender)");
                System.out.println("same = " + Arrays.equals(buffer1, buffer2));
            } else {
                final byte[] buffer = new byte[TEST_BYTES_COUNT];
                int read = 0;
                while (read < TEST_BYTES_COUNT) {
                    read += inputStream.read(buffer, read, buffer.length - read);
//                    logger.log(Level.INFO, "Local job read: " + read);
                }

                logger.log(Level.INFO, "now writing");
                outputStream.write(buffer);
                outputStream.flush();
                logger.log(Level.INFO, "finished writing after reading (Reader)");
            }
        }

        @Override
        public void exception(final IOException e) {
            System.out.println("got EXCEPTION e = " + e);
        }

        @Override
        public void p2pFailed() {
            System.out.println("P2P failed");
        }
    }
}
