package controller;

import org.ice4j.Transport;
import org.ice4j.TransportAddress;
import org.ice4j.ice.*;
import org.ice4j.ice.sdp.CandidateAttribute;
import org.ice4j.ice.sdp.IceSdpUtils;
import org.opentelecoms.javax.sdp.NistSdpFactory;

import javax.sdp.*;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * Created on 4/1/20
 */
final class SdpUtils {
    /**
     * Creates a session description containing the streams from the specified
     * <tt>agent</tt> using dummy codecs. This method is unlikely to be of use
     * to integrating applications as they would likely just want to feed a
     * {@link MediaDescription} and have it populated with all the necessary
     * attributes.
     *
     * @param agent the {@link Agent} we'd like to generate.
     * @return a {@link SessionDescription} representing <tt>agent</tt>'s
     * streams.
     * @throws Throwable on rainy days
     */
    public static String createSDPDescription(final Agent agent) throws Throwable {
        final SdpFactory factory = new NistSdpFactory();
        final SessionDescription sdess = factory.createSessionDescription();
        IceSdpUtils.initSessionDescription(sdess, agent);
        return sdess.toString();
    }

    /**
     * Configures <tt>localAgent</tt> the the remote peer streams, components,
     * and candidates specified in <tt>sdp</tt>
     *
     * @param localAgent the {@link Agent} that we'd like to configure.
     * @param sdp        the SDP string that the remote peer sent.
     * @throws Exception for all sorts of reasons.
     */
    @SuppressWarnings("unchecked") // jain-sdp legacy code.
    public static void parseSDP(Agent localAgent, String sdp) throws Exception {
        final SdpFactory factory = new NistSdpFactory();
        final SessionDescription sdess = factory.createSessionDescription(sdp);

        for (final IceMediaStream stream : localAgent.getStreams()) {
            stream.setRemotePassword(sdess.getAttribute("ice-pwd"));
            stream.setRemoteUfrag(sdess.getAttribute("ice-ufrag"));
        }

        Connection globalConn = sdess.getConnection();
        String globalConnAddr = null;
        if (globalConn != null) {
            globalConnAddr = globalConn.getAddress();
        }

        final Vector<MediaDescription> mdescs = sdess.getMediaDescriptions(true);

        for (final MediaDescription desc : mdescs) {
            final String streamName = desc.getMedia().getMediaType();
            final IceMediaStream stream = localAgent.getStream(streamName);
            if (stream == null) {
                continue;
            }

            final Vector<Attribute> attributes = desc.getAttributes(true);
            for (final Attribute attribute : attributes) {
                if (attribute.getName().equals(CandidateAttribute.NAME)) {
                    parseCandidate(attribute, stream);
                }
            }

            //set default candidates
            final Connection streamConn = desc.getConnection();
            final String streamConnAddr = streamConn != null ? streamConn.getAddress() : globalConnAddr;

            final int port = desc.getMedia().getMediaPort();

            final TransportAddress defaultRtpAddress = new TransportAddress(streamConnAddr, port, Transport.UDP);

            int rtcpPort = port + 1;
            final String rtcpAttributeValue = desc.getAttribute("rtcp");

            if (rtcpAttributeValue != null) {
                rtcpPort = Integer.parseInt(rtcpAttributeValue);
            }

            final TransportAddress defaultRtcpAddress = new TransportAddress(streamConnAddr, rtcpPort, Transport.UDP);
            final Component rtpComponent = stream.getComponent(Component.RTP);
            final Component rtcpComponent = stream.getComponent(Component.RTCP);

            final Candidate<?> defaultRtpCandidate = rtpComponent.findRemoteCandidate(defaultRtpAddress);
            rtpComponent.setDefaultRemoteCandidate(defaultRtpCandidate);

            if (rtcpComponent != null) {
                final Candidate<?> defaultRtcpCandidate = rtcpComponent.findRemoteCandidate(defaultRtcpAddress);
                rtcpComponent.setDefaultRemoteCandidate(defaultRtcpCandidate);
            }
        }
    }

    /**
     * Parses the <tt>attribute</tt>.
     *
     * @param attribute the attribute that we need to parse.
     * @param stream    the {@link IceMediaStream} that the candidate is supposed
     *                  to belong to.
     * @return a newly created {@link RemoteCandidate} matching the
     * content of the specified <tt>attribute</tt> or <tt>null</tt> if the
     * candidate belonged to a component we don't have.
     */
    private static RemoteCandidate parseCandidate(Attribute attribute, IceMediaStream stream) {
        String value = null;

        try {
            value = attribute.getValue();
        } catch (Throwable t) {
        }//can't happen

        StringTokenizer tokenizer = new StringTokenizer(value);

        //XXX add exception handling.
        String foundation = tokenizer.nextToken();
        int componentID = Integer.parseInt(tokenizer.nextToken());
        Transport transport = Transport.parse(tokenizer.nextToken());
        long priority = Long.parseLong(tokenizer.nextToken());
        String address = tokenizer.nextToken();
        int port = Integer.parseInt(tokenizer.nextToken());

        TransportAddress transAddr = new TransportAddress(address, port, transport);

        tokenizer.nextToken(); //skip the "typ" String
        CandidateType type = CandidateType.parse(tokenizer.nextToken());

        Component component = stream.getComponent(componentID);

        if (component == null) {
            return null;
        }

        // check if there's a related address property

        RemoteCandidate relatedCandidate = null;
        if (tokenizer.countTokens() >= 4) {
            tokenizer.nextToken(); // skip the raddr element
            String relatedAddr = tokenizer.nextToken();
            tokenizer.nextToken(); // skip the rport element
            int relatedPort = Integer.parseInt(tokenizer.nextToken());
            TransportAddress raddr = new TransportAddress(relatedAddr, relatedPort, Transport.UDP);
            relatedCandidate = component.findRemoteCandidate(raddr);
        }

        RemoteCandidate cand = new RemoteCandidate(transAddr, component, type, foundation, priority, relatedCandidate);
        component.addRemoteCandidate(cand);
        return cand;
    }
}