package controller;

/**
 * Created on 4/1/20
 */

import org.ice4j.TransportAddress;
import org.ice4j.ice.Agent;
import org.ice4j.ice.CandidatePair;
import org.ice4j.ice.Component;
import org.ice4j.ice.IceMediaStream;
import org.ice4j.ice.IceProcessingState;
import org.ice4j.socket.IceSocketWrapper;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;

public class StateListener implements PropertyChangeListener {
    private InetAddress hostname;
    int port;

    public void propertyChange(final PropertyChangeEvent evt) {
        if (evt.getSource() instanceof Agent) {
            final Agent agent = (Agent) evt.getSource();
            if (agent.getState().equals(IceProcessingState.TERMINATED)) {
                // Your agent is connected. Terminated means ready to communicate
                for (final IceMediaStream stream : agent.getStreams()) {
                    if (stream.getName().contains("useetoo")) {
                        final Component rtpComponent = stream.getComponent(org.ice4j.ice.Component.RTP);
                        final CandidatePair rtpPair = rtpComponent.getSelectedPair();
                        // We use IceSocketWrapper, but you can just use the UDP socket
                        // The advantage is that you can change the protocol from UDP to TCP easily
                        // Currently only UDP exists so you might not need to use the wrapper.
                        final IceSocketWrapper wrapper = rtpPair.getIceSocketWrapper();
                        // Get information about remote address for packet settings
                        final TransportAddress ta = rtpPair.getRemoteCandidate().getTransportAddress();
                        hostname = ta.getAddress();
                        port = ta.getPort();
                        System.out.println("hostname = " + hostname);
                        System.out.println("port = " + port);

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                   DatagramPacket packet = new DatagramPacket(new byte[1000], 1000);
                                try {
                                    wrapper.receive(packet);
                                    System.out.println("received");
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }).start();

                        DatagramPacket packet = new DatagramPacket(new byte[1000], 1000);
                        packet.setAddress(hostname);
                        packet.setPort(port);
                        try {
                            wrapper.send(packet);
                            System.out.println("sent");

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}

