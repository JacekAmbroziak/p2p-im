package controller;

import io.socket.client.IO;
import io.socket.client.Socket;
import org.ice4j.Transport;
import org.ice4j.TransportAddress;
import org.ice4j.ice.*;
import org.ice4j.ice.harvest.StunCandidateHarvester;
import org.ice4j.ice.harvest.TurnCandidateHarvester;
import org.ice4j.pseudotcp.PseudoTcpSocket;
import org.ice4j.pseudotcp.PseudoTcpSocketFactory;
import org.ice4j.security.LongTermCredential;
import org.json.JSONObject;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * User: jacek
 * Date: 4/11/20
 * Time: 12:47 PM
 * <p/>
 * Copyright 2020 Ambrosoft, Inc. All Rights Reserved.
 * This software is the proprietary information of Ambrosoft, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
final class P2PConnector {
    private static final Logger logger = Logger.getLogger(P2PConnector.class.getName());
    private static final boolean USE_STUN = true;
    private static final boolean USE_TURN = true;
    private static final int PORT_NUMBER = 6543;
    static final String SIGNALLING_URI = "http://fast-bastion-38385.herokuapp.com/chat";

    private final String localId;
    private final String remoteId;
    private final Agent agent;
    private boolean isControlling;

    P2PConnector(final String localId, final String remoteId) throws Throwable {
        this.localId = localId;
        this.remoteId = remoteId;
        // IPv6 addresses didn't seem to work in testing
        System.setProperty("org.ice4j.ipv6.DISABLED", "true");

        agent = createAgent(PORT_NUMBER);
        agent.setControlling(isControlling = Utils.isControlling(localId, remoteId));
        if (isControlling) {
            agent.setNominationStrategy(NominationStrategy.NOMINATE_HIGHEST_PRIO);
        }
    }

    public void connect(final Connectable connectable) throws P2PException {
        try {
            agent.addStateChangeListener(new IceProcessingListener(connectable));
            connect(agent, localId, remoteId);
        } catch (Exception e) {
            throw new P2PException(e);
        }
    }

    static final class P2PException extends Exception {
        public P2PException(final Throwable cause) {
            super(cause);
        }
    }

    private static void connect(final Agent agent, final String localId, final String remoteId) throws InterruptedException, URISyntaxException {
        // this is just a synchronization device
        final BlockingQueue<Object> values = new LinkedBlockingQueue<>();
        final Socket socket = IO.socket(SIGNALLING_URI, Utils.createOptions());
        socket.on(Socket.EVENT_CONNECT, objects -> {
            try {
                final JSONObject intro = new JSONObject();
                intro.put("type", "P2P");
                intro.put("fromId", localId);
                intro.put("toId", remoteId);
                socket.emit("intro", intro);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).on("user connected", objs -> {
            System.out.println("user connected = " + Arrays.toString(objs));
            try {
                final String localSDP = test.SdpUtils.createSDPDescription(agent);
//                System.out.println("localSDP = " + localSDP);
//                System.out.println("sending offer ...");
                socket.emit("offer", localId, remoteId, localSDP);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }).on("offer forward", objs -> {
//            System.out.println("offer forward = " + Arrays.toString(objs));
//            System.out.println("answering ...");
            try {
                final String remoteSdp = (String) objs[2];
//                System.out.println(remoteSdp);
                controller.SdpUtils.parseSDP(agent, remoteSdp);
                final String localSDP = test.SdpUtils.createSDPDescription(agent);
//                System.out.println("localSDP = " + localSDP);
                socket.emit("answer", localId, remoteId, localSDP);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
            // main thread will be able to continue
            values.offer("done");
        }).on("answer forward", objs -> {
            System.out.println("answer forward = " + Arrays.toString(objs));
//                            socket.emit("answer", myEmail, remoteEmail, "answer by " + myEmail);
            try {
                final String remoteSdp = (String) objs[2];
                System.out.println(remoteSdp);
                controller.SdpUtils.parseSDP(agent, remoteSdp);
            } catch (Exception e) {
                e.printStackTrace();
            }
            // main thread will be able to continue
            values.offer("done");
        }).on("response", objs -> System.out.println("response args = " + Arrays.toString(objs)))
                .on("message", objs -> System.out.println("message args = " + Arrays.toString(objs)));

        socket.connect();
        // the below call will block until handshake above is completed and peers have exchanged SDPs
        values.take();
//        System.out.println("after take; starting connectivity");

        agent.startConnectivityEstablishment();
        // TODO are all of these needed?
        socket.off();
        socket.disconnect();
        socket.close();
    }

    private class PseudoTcpEndPoint {
        private final DatagramSocket datagramSocket;
        private final InetSocketAddress peerAddress;
        private PseudoTcpSocket pseudoTcpSocket;

        PseudoTcpEndPoint(final DatagramSocket socket, final InetSocketAddress peerAddress) {
            this.datagramSocket = socket;
            this.peerAddress = peerAddress;
        }

        void start(final Connectable connectable) throws IOException {
            final PseudoTcpSocket socket = new PseudoTcpSocketFactory().createSocket(datagramSocket);
            socket.setConversationID(0b0100_0000_0000_0000_0000_0000_0000_0000);
            socket.setMTU(1500);
            socket.setDebugName(isControlling ? "R" : "L");
            pseudoTcpSocket = socket;
            logger.log(Level.INFO, "PseudoTcpEndPoint: start called");
            if (isControlling) {
                socket.connect(peerAddress, 9000);
            } else {
                socket.accept(9000);
            }
            connectable.connected(socket, isControlling);
        }

        void close() throws IOException {
            if (pseudoTcpSocket != null) {
                pseudoTcpSocket.close();
            }
        }
    }

    private final class IceProcessingListener implements PropertyChangeListener {
        private final Connectable connectable;
        private PseudoTcpEndPoint endPoint;

        IceProcessingListener(final Connectable connectable) {
            this.connectable = connectable;
        }

        public void propertyChange(final PropertyChangeEvent evt) {
            final Object iceProcessingState = evt.getNewValue();
            logger.log(Level.INFO, "Local agent entered the " + iceProcessingState + " state.");
            if (iceProcessingState == IceProcessingState.COMPLETED) {
                final Agent agent = (Agent) evt.getSource();
                logger.log(Level.INFO, "Local: Create pseudo tcp stream");
                final IceMediaStream dataStream = agent.getStream("data");
                final Component udpComponent = dataStream.getComponents().get(0);
                final CandidatePair selectedPair = udpComponent.getSelectedPair();
                if (selectedPair != null) {
                    final LocalCandidate localCandidate = selectedPair.getLocalCandidate();
                    final Candidate<?> remoteCandidate = selectedPair.getRemoteCandidate();
                    logger.log(Level.INFO, "Local: " + localCandidate);
                    logger.log(Level.INFO, "Remote: " + remoteCandidate);
//                    readerPseudoTcpJob = new Connect.ReaderPseudoTcpJob(selectedPair.getDatagramSocket());
                    endPoint = new PseudoTcpEndPoint(selectedPair.getDatagramSocket(), remoteCandidate.getTransportAddress());
                } else {
                    logger.log(Level.INFO, "Failed to select any candidate pair");
                }
            } else if (iceProcessingState == IceProcessingState.TERMINATED) {
                if (endPoint != null) {
                    try {
                        endPoint.start(connectable);
                    } catch (IOException e) {
                        connectable.exception(e);
                    }
                }
//                synchronized (localAgentMonitor) {
//                    localAgentMonitor.notifyAll();
//                }
            } else if (iceProcessingState == IceProcessingState.FAILED) {
//                synchronized (localAgentMonitor) {
//                    localAgentMonitor.notifyAll();
//                }
                connectable.p2pFailed();
            }
        }
    }

    private static Agent createAgent(final int pTcpPort) throws Throwable {
        final Agent agent = new Agent();
        if (USE_STUN) {
            // pairs of URI:port
            final LinkedHashMap<String, Integer> stuns = new LinkedHashMap<>();
            stuns.put("stun.l.google.com", 19302);
//            stuns.put("stun1.l.google.com", 19302);
//            stuns.put("stun2.l.google.com", 19302);
//            stuns.put("stun3.l.google.com", 19302);
//            stuns.put("stun4.l.google.com", 19302);

            for (final Map.Entry<String, Integer> stun : stuns.entrySet()) {
                try {
                    agent.addCandidateHarvester(new StunCandidateHarvester(new TransportAddress(InetAddress.getByName(stun.getKey()), stun.getValue(), Transport.UDP)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (USE_TURN) {
            final String[] hostnames = new String[]{"130.79.90.150", "2001:660:4701:1001:230:5ff:fe1a:805f"};
            int port = 3478;
            final LongTermCredential longTermCredential = new LongTermCredential("guest", "anonymouspower!!");
            for (final String hostname : hostnames) {
                agent.addCandidateHarvester(new TurnCandidateHarvester(new TransportAddress(hostname, port, Transport.UDP), longTermCredential));
            }
        }
        createStream(pTcpPort, "data", agent);
        return agent;
    }

    private static IceMediaStream createStream(int pTcpPort, String streamName, Agent agent) throws Throwable {
        IceMediaStream stream = agent.createMediaStream(streamName);
        long startTime = System.currentTimeMillis();
        // udp component
        agent.createComponent(stream, Transport.UDP, pTcpPort, pTcpPort, pTcpPort + 100);
        long endTime = System.currentTimeMillis();
        logger.log(Level.INFO, "UDP Component created in " + (endTime - startTime) + " ms");
        return stream;
    }
}
