/*
 * ice4j, the OpenSource Java Solution for NAT and Firewall Traversal.
 *
 * Copyright @ 2015 Atlassian Pty Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package controller;

import org.ice4j.Transport;
import org.ice4j.TransportAddress;
import org.ice4j.ice.*;
import org.ice4j.ice.harvest.StunCandidateHarvester;
import org.ice4j.ice.harvest.TurnCandidateHarvester;
import org.ice4j.pseudotcp.PseudoTcpSocket;
import org.ice4j.pseudotcp.PseudoTcpSocketFactory;
import org.ice4j.security.LongTermCredential;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Sample program which first uses ICE to discover UDP connectivity. After that
 * selected cadidates are used by "remote" and "local" pseudoTCP peers to
 * transfer some test data.
 *
 * @author Pawel Domas
 */
public class IcePseudoTcp {
    /**
     * The logger.
     */
    private static final Logger logger = Logger.getLogger(IcePseudoTcp.class.getName());
    private static long startTime;
    /**
     * Local job thread variable
     */
    private static ReaderPseudoTcpJob localJob;
    /**
     * Remote job thread variable
     */
    private static SenderPseudoTcpJob remoteJob;
    /**
     * Test data size
     */
    private static final int TEST_BYTES_COUNT = 15000000;
    /**
     * Flag inidcates if STUN should be used
     */
    private static final boolean USE_STUN = true;
    /**
     * Flag inidcates if TURN should be used
     */
    private static final boolean USE_TURN = true;
    /**
     * Monitor object used to wait for remote agent to finish it's job
     */
    private static final Object remoteAgentMonitor = new Object();
    /**
     * Monitor object used to wait for local agent to finish it's job
     */
    private static final Object localAgentMonitor = new Object();
    /**
     * Timeout for ICE discovery
     */
    private static long agentJobTimeout = 15000;

    static Agent createAgent(final int pTcpPort) throws Throwable {
        final Agent agent = new Agent();
        if (USE_STUN) {
            // pairs of URI:port
            final LinkedHashMap<String, Integer> stuns = new LinkedHashMap<>();
            stuns.put("stun.l.google.com", 19302);
//            stuns.put("stun1.l.google.com", 19302);
//            stuns.put("stun2.l.google.com", 19302);
//            stuns.put("stun3.l.google.com", 19302);
//            stuns.put("stun4.l.google.com", 19302);

            for (final Map.Entry<String, Integer> stun : stuns.entrySet()) {
                try {
                    agent.addCandidateHarvester(new StunCandidateHarvester(new TransportAddress(InetAddress.getByName(stun.getKey()), stun.getValue(), Transport.UDP)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (USE_TURN) {
            final String[] hostnames = new String[]{"130.79.90.150", "2001:660:4701:1001:230:5ff:fe1a:805f"};
            int port = 3478;
            final LongTermCredential longTermCredential = new LongTermCredential("guest", "anonymouspower!!");
            for (final String hostname : hostnames) {
                agent.addCandidateHarvester(new TurnCandidateHarvester(new TransportAddress(hostname, port, Transport.UDP), longTermCredential));
            }
        }
        createStream(pTcpPort, "data", agent);
        return agent;
    }

    private static IceMediaStream createStream(int pTcpPort, String streamName, Agent agent) throws Throwable {
        IceMediaStream stream = agent.createMediaStream(streamName);
        long startTime = System.currentTimeMillis();
        // udp component
        agent.createComponent(stream, Transport.UDP, pTcpPort, pTcpPort, pTcpPort + 100);
        long endTime = System.currentTimeMillis();
        logger.log(Level.INFO, "UDP Component created in " + (endTime - startTime) + " ms");
        return stream;
    }

    static final class LocalIceProcessingListener implements PropertyChangeListener {
        /**
         * System.exit()s as soon as ICE processing enters a final state.
         *
         * @param evt the {@link PropertyChangeEvent} containing the old and new
         *            states of ICE processing.
         */
        public void propertyChange(PropertyChangeEvent evt) {
            long processingEndTime = System.currentTimeMillis();

            final Object iceProcessingState = evt.getNewValue();

            logger.log(Level.INFO, "Local agent entered the " + iceProcessingState + " state.");
            if (iceProcessingState == IceProcessingState.COMPLETED) {
                logger.log(Level.INFO, "Local - Total ICE processing time: " + (processingEndTime - startTime) + "ms");
                Agent agent = (Agent) evt.getSource();
                logger.log(Level.INFO, "Local: Create pseudo tcp stream");
                IceMediaStream dataStream = agent.getStream("data");
                Component udpComponent = dataStream.getComponents().get(0);
                CandidatePair selectedPair = udpComponent.getSelectedPair();
                if (selectedPair != null) {
                    LocalCandidate localCandidate = selectedPair.getLocalCandidate();
                    Candidate<?> remoteCandidate = selectedPair.getRemoteCandidate();
                    logger.log(Level.INFO, "Local: " + localCandidate);
                    logger.log(Level.INFO, "Remote: " + remoteCandidate);
                    try {
                        localJob = new ReaderPseudoTcpJob(selectedPair.getDatagramSocket());
                    } catch (UnknownHostException ex) {
                        logger.log(Level.SEVERE, "Error while trying to create local pseudotcp thread " + ex);
                    }
                } else {
                    logger.log(Level.INFO, "Failed to select any candidate pair");
                }
            } else {
                if (iceProcessingState == IceProcessingState.TERMINATED || iceProcessingState == IceProcessingState.FAILED) {
                    /*
                     * Though the process will be instructed to die, demonstrate
                     * that Agent instances are to be explicitly prepared for
                     * garbage collection.
                     */
                    if (localJob != null && iceProcessingState == IceProcessingState.TERMINATED) {
                        localJob.start();
                    }
                    synchronized (localAgentMonitor) {
                        localAgentMonitor.notifyAll();
                    }
                }
            }
        }
    }

    static final class RemoteIceProcessingListener implements PropertyChangeListener {
        /**
         * System.exit()s as soon as ICE processing enters a final state.
         *
         * @param evt the {@link PropertyChangeEvent} containing the old and new
         *            states of ICE processing.
         */
        public void propertyChange(final PropertyChangeEvent evt) {
            final long processingEndTime = System.currentTimeMillis();
            final Object iceProcessingState = evt.getNewValue();

            logger.log(Level.INFO, "Remote agent entered the " + iceProcessingState + " state.");
            if (iceProcessingState == IceProcessingState.COMPLETED) {
                logger.log(Level.INFO, "COMPLETED Remote: Total ICE processing time: " + (processingEndTime - startTime) + " ms ");
                Agent agent = (Agent) evt.getSource();

                logger.log(Level.INFO, "Remote: Create pseudo tcp stream");
                IceMediaStream dataStream = agent.getStream("data");
                Component udpComponent = dataStream.getComponents().get(0);
                CandidatePair usedPair = udpComponent.getSelectedPair();
                if (usedPair != null) {
                    LocalCandidate localCandidate = usedPair.getLocalCandidate();
                    Candidate<?> remoteCandidate = usedPair.getRemoteCandidate();
                    logger.log(Level.INFO, "Remote: Local address " + localCandidate);
                    logger.log(Level.INFO, "Remote: Peer address " + remoteCandidate);
                    try {
                        remoteJob = new SenderPseudoTcpJob(usedPair.getDatagramSocket(), remoteCandidate.getTransportAddress());
                    } catch (UnknownHostException ex) {
                        logger.log(Level.SEVERE, "Error while trying to create remote pseudotcp thread " + ex);
                    }
                } else {
                    logger.log(Level.SEVERE, "Remote: Failed to select any candidate pair");
                }
            } else if (iceProcessingState == IceProcessingState.TERMINATED || iceProcessingState == IceProcessingState.FAILED) {
                /*
                 * Though the process will be instructed to die, demonstrate
                 * that Agent instances are to be explicitly prepared for
                 * garbage collection.
                 */
                if (remoteJob != null && iceProcessingState == IceProcessingState.TERMINATED) {
                    remoteJob.start();
                }
                synchronized (remoteAgentMonitor) {
                    remoteAgentMonitor.notifyAll();
                }
            }
        }
    }

    public static void main(String[] args) throws Throwable {
        final String myEmail = args[0];
        final String remoteEmail = args[1];

        startTime = System.currentTimeMillis();

        final boolean local;
        {   // make lexicographically smaller the controlling peer
            final int compare = myEmail.compareTo(remoteEmail);
            if (compare == 0) {
                throw new IllegalArgumentException("to and from cannot be the same");
            } else {
                local = compare < 0;
            }
        }

        int localPort = 7999;
//        int remotePort = 6000;

        final Agent agent;
        if (local) {
            Agent localAgent = createAgent(localPort);
            localAgent.setNominationStrategy(NominationStrategy.NOMINATE_HIGHEST_PRIO);
            localAgent.addStateChangeListener(new IcePseudoTcp.LocalIceProcessingListener());
            localAgent.setControlling(true);
            agent = localAgent;
        } else {
            Agent remotePeer = createAgent(localPort);
            remotePeer.addStateChangeListener(new IcePseudoTcp.RemoteIceProcessingListener());
            remotePeer.setControlling(false);
            agent = remotePeer;
        }

        long endTime = System.currentTimeMillis();

        /*
            this is the original code

            below I will implement the same semantics, hopefully, using serialized session descriptions
            exchanged with the help of the external server

        // transfer from remote to local
        Ice.transferRemoteCandidates(localAgent, remotePeer);
        // local streams get remote Ufrag and Password
        final String remoteUfrag = remotePeer.getLocalUfrag();
        final String remotePassword = remotePeer.getLocalPassword();
        for (final IceMediaStream stream : agent.getStreams()) {
            stream.setRemoteUfrag(remoteUfrag);
            stream.setRemotePassword(remotePassword);
        }
         */

        final String mySdp = SdpUtils.createSDPDescription(agent);
        final String remoteSdp = Signalling.tradeSdpStringsWithRemote(mySdp, myEmail, remoteEmail);
        SdpUtils.parseSDP(agent, remoteSdp); // This will add the remote information to the agent.


        // transfer from local to remote: this will happen in the other instance so commenting out
        /*
        Ice.transferRemoteCandidates(remotePeer, localAgent);
        for (final IceMediaStream stream : remotePeer.getStreams()) {
            stream.setRemoteUfrag(localAgent.getLocalUfrag());
            stream.setRemotePassword(localAgent.getLocalPassword());
        }
        */

        logger.log(Level.INFO, "Total candidate gathering time: {0} ms", (endTime - startTime));
        logger.log(Level.INFO, "Agent: {0}", agent);

        agent.startConnectivityEstablishment();

        IceMediaStream dataStream = agent.getStream("data");

        if (dataStream != null) {
            logger.log(Level.INFO, "Local data clist:" + dataStream.getCheckList());
        }
        //wait for one of the agents to complete it's job 
//        synchronized (remoteAgentMonitor) {
//            remoteAgentMonitor.wait(agentJobTimeout);
//        }
        if (remoteJob != null) {
            logger.log(Level.FINEST, "Remote thread join started");
            remoteJob.join();
            logger.log(Level.FINEST, "Remote thread joined");
        }
        if (localJob != null) {
            logger.log(Level.FINEST, "Local thread join started");
            localJob.join();
            logger.log(Level.FINEST, "Local thread joined");
        }
//        agent.free();
//        System.exit(0);
    }

    private static class ReaderPseudoTcpJob extends Thread implements Runnable {
        private DatagramSocket datagramSocket;

        ReaderPseudoTcpJob(DatagramSocket socket) throws UnknownHostException {
            this.datagramSocket = socket;
        }

        @Override
        public void run() {
            logger.log(Level.FINEST, "Local PSEUDO worker started");
            try {
                logger.log(Level.INFO, "Local PSEUDOTCP is using: " + datagramSocket.getLocalSocketAddress() + datagramSocket);

//                DatagramPacket packet = new DatagramPacket(new byte[1000], 1000);
//                datagramSocket.receive(packet);
//                logger.log(Level.INFO, "Local PSEUDOTCP received DATA");


                PseudoTcpSocket socket = new PseudoTcpSocketFactory().createSocket(datagramSocket);
                socket.setConversationID(0b0100_0000_0000_0000_0000_0000_0000_0000);
                socket.setMTU(1500);
                socket.setDebugName("L");
                logger.log(Level.FINEST, "before accept");

                socket.accept(9000);
                logger.log(Level.FINEST, "after read");

                byte[] buffer = new byte[TEST_BYTES_COUNT];
                int read = 0;
                while (read != TEST_BYTES_COUNT) {
                    read += socket.getInputStream().read(buffer);
                    logger.log(Level.FINEST, "Local job read: " + read);
                }
                //TODO: close when all received data is acked
                socket.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            logger.log(Level.FINEST, "Local pseudotcp worker finished");
        }
    }

    private static class SenderPseudoTcpJob extends Thread implements Runnable {
        private DatagramSocket datagramSocket;
        private InetSocketAddress peerAddr;

        SenderPseudoTcpJob(DatagramSocket socket, InetSocketAddress peerAddr) throws UnknownHostException {
            this.datagramSocket = socket;
            this.peerAddr = peerAddr;
        }

        @Override
        public void run() {
            logger.log(Level.FINEST, "Remote PSEUDO worker started");
            try {

                logger.log(Level.INFO, "Remote PSEUDOTCP is using: " + datagramSocket.getLocalSocketAddress() + " and will communicate with: " + peerAddr);

//                DatagramPacket packet = new DatagramPacket(new byte[1000], 1000);
//                packet.setAddress(peerAddr.getAddress());
//                datagramSocket.send(packet);
//                logger.log(Level.INFO, "Remote PSEUDOTCP sent DATA");


                PseudoTcpSocket socket = new PseudoTcpSocketFactory().createSocket(datagramSocket);
                socket.setConversationID(0b0100_0000_0000_0000_0000_0000_0000_0000);
                socket.setMTU(1500);
                socket.setDebugName("R");
                long start, end;
                start = System.currentTimeMillis();
                socket.connect(peerAddr, 9000);
                byte[] buffer = new byte[TEST_BYTES_COUNT];
                socket.getOutputStream().write(buffer);
                socket.getOutputStream().flush();
                //Socket will be closed by the iceAgent
                socket.close();
                end = System.currentTimeMillis();
                logger.log(Level.INFO, "Transferred " + TEST_BYTES_COUNT + " bytes in " + ((end - start) / 1000) + " sec");

            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            logger.log(Level.FINEST, "Remote pseudotcp worker finished");
        }
    }
}
