package controller;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.ice4j.Transport;
import org.ice4j.TransportAddress;
import org.ice4j.ice.*;
import org.ice4j.ice.harvest.StunCandidateHarvester;
import org.ice4j.ice.harvest.TurnCandidateHarvester;
import org.ice4j.pseudotcp.PseudoTcpSocket;
import org.ice4j.pseudotcp.PseudoTcpSocketFactory;
import org.ice4j.security.LongTermCredential;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.Duration;
import java.time.Instant;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created on 4/1/20
 */
final class ICETest {
    private static final Logger logger = Logger.getLogger(ICETest.class.getName());
    /**
     * Flag inidcates if STUN should be used
     */
    private static final boolean USE_STUN = true;
    /**
     * Flag inidcates if TURN should be used
     */
    private static final boolean USE_TURN = true;
    private static long startTime;
    /**
     * Local job thread variable
     */
    private static LocalPseudoTcpJob localJob;
    /**
     * Monitor object used to wait for local agent to finish it's job
     */
    private static final Object localAgentMonitor = new Object();
    /**
     * Test data size
     */
    private static final int TEST_BYTES_COUNT = 15000000;

    public static void main(String[] args) throws Throwable {
        final String myEmail = args[0];
        final String remoteEmail = args[1];
        final Agent agent = createAgent(7999);
        {   // make lexicographically smaller the controlling peer
            final int compare = myEmail.compareTo(remoteEmail);
            if (compare == 0) {
                throw new IllegalArgumentException("to and from cannot be the same");
            } else {
                agent.setControlling(compare < 0);  // arbitrarily preceding wins
                agent.setNominationStrategy(NominationStrategy.NOMINATE_HIGHEST_PRIO);
            }
        }
        // ensure no trickling: all candidates at once
        {
            final boolean trickling = agent.isTrickling();
            System.out.println("trickling = " + trickling);
            if (trickling) {
                agent.setTrickling(false);
            }
        }


        try (final CloseableHttpClient httpclient = HttpClients.createDefault()) {
            int attempts = 4;
            SdpInfo remoteSdp;
            do {
//                final String mySdp = getSdpFromIce(agent);
                final String mySdp = SdpUtils.createSDPDescription(agent);
                Instant startTime = Instant.now();
                // post ours
                Signalling.postSdp(httpclient, myEmail, mySdp);
                // wait a bit
                Thread.sleep(3000);

                System.out.println("attempts = " + attempts + " startTime = " + startTime);

                // get remote sdp
                remoteSdp = Signalling.getSdp(httpclient, remoteEmail);

                System.out.println("remoteSdp time = " + remoteSdp.instant);
                if (remoteSdp.instant.isAfter(startTime)) {
                    // very good: remote is even newer
                    break;
                } else if (Duration.between(remoteSdp.instant, startTime).compareTo(Duration.ofMinutes(1)) < 0) {
                    // remote is older but less than 1 minute: also good
                    break;
                } else {
                    // continue
                }
            } while (--attempts > 0);

            SdpUtils.parseSDP(agent, remoteSdp.sdpText); // This will add the remote information to the agent.

            agent.addStateChangeListener(new LocalIceProcessingListener());


//            agent.addStateChangeListener(new StateListener());
            agent.startConnectivityEstablishment(); // This will do all the work for you to connect

            IceMediaStream dataStream = agent.getStream("data");

            if (dataStream != null) {
                logger.log(Level.INFO, "Local data clist:" + dataStream.getCheckList());
            }

            if (localJob != null) {
                logger.log(Level.FINEST, "Local thread join started");
                localJob.join();
                logger.log(Level.FINEST, "Local thread joined");
            }
            agent.free();
            System.exit(0);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    protected static Agent createAgent(final int pTcpPort) throws Throwable {
        final Agent agent = new Agent();
        // STUN
        if (USE_STUN) {
            StunCandidateHarvester stunHarv = new StunCandidateHarvester(new TransportAddress("sip-communicator.net", 3478, Transport.UDP));
            StunCandidateHarvester stun6Harv = new StunCandidateHarvester(new TransportAddress("ipv6.sip-communicator.net", 3478, Transport.UDP));

            agent.addCandidateHarvester(stunHarv);
            agent.addCandidateHarvester(stun6Harv);
        }
        // TURN
        if (USE_TURN) {
            String[] hostnames = new String[]{"130.79.90.150", "2001:660:4701:1001:230:5ff:fe1a:805f"};
            int port = 3478;
            LongTermCredential longTermCredential = new LongTermCredential("guest", "anonymouspower!!");

            for (String hostname : hostnames) {
                agent.addCandidateHarvester(new TurnCandidateHarvester(new TransportAddress(hostname, port, Transport.UDP), longTermCredential));
            }
        }
        //STREAM
        createStream(pTcpPort, "data", agent);
        return agent;
    }

    private static IceMediaStream createStream(int pTcpPort, String streamName, Agent agent) throws Throwable {
        IceMediaStream stream = agent.createMediaStream(streamName);
        long startTime = System.currentTimeMillis();
        // udp component
        agent.createComponent(stream, Transport.UDP, pTcpPort, pTcpPort, pTcpPort + 100);
        logger.log(Level.INFO, "UDP Component created in " + (System.currentTimeMillis() - startTime) + " ms");
        return stream;
    }

    static String getSdpFromIce(final Agent agent) throws Throwable {
        /* Setup the STUN servers: */
        final String[] hostnames = new String[]{"jitsi.org", "numb.viagenie.ca", "stun.ekiga.net"};
        // Look online for actively working public STUN Servers. You can find free servers.
        // Now add these URLS as Stun Servers with standard 3478 port for STUN servrs.
        for (final String hostname : hostnames) {
            try {
                // InetAddress qualifies a url to an IP Address, if you have an error here, make sure the url is reachable and correct
                TransportAddress ta = new TransportAddress(InetAddress.getByName(hostname), 3478, Transport.UDP);
                // Currently Ice4J only supports UDP and will throw an Error otherwise
                agent.addCandidateHarvester(new StunCandidateHarvester(ta));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        final IceMediaStream stream = agent.createMediaStream("data");
        int port = 6000; // Choose any port
        agent.createComponent(stream, Transport.UDP, port, port, port + 100);
        // The three last arguments are: preferredPort, minPort, maxPort

        return SdpUtils.createSDPDescription(agent);
    }

    private static final class LocalIceProcessingListener
            implements PropertyChangeListener {
        /**
         * System.exit()s as soon as ICE processing enters a final state.
         *
         * @param evt the {@link PropertyChangeEvent} containing the old and new
         *            states of ICE processing.
         */
        public void propertyChange(PropertyChangeEvent evt) {
            long processingEndTime = System.currentTimeMillis();

            final Object iceProcessingState = evt.getNewValue();

            logger.log(Level.INFO, "Local agent entered the " + iceProcessingState + " state.");
            if (iceProcessingState == IceProcessingState.COMPLETED) {
                logger.log(Level.INFO, "Local - Total ICE processing time: " + (processingEndTime - startTime) + "ms");
                Agent agent = (Agent) evt.getSource();
                logger.log(Level.INFO, "Local: Create pseudo tcp stream");
                IceMediaStream dataStream = agent.getStream("data");
                Component udpComponent = dataStream.getComponents().get(0);
                CandidatePair selectedPair = udpComponent.getSelectedPair();
                if (selectedPair != null) {
                    LocalCandidate localCandidate = selectedPair.getLocalCandidate();
                    Candidate<?> remoteCandidate = selectedPair.getRemoteCandidate();
                    logger.log(Level.INFO, "Local: " + localCandidate);
                    logger.log(Level.INFO, "Remote: " + remoteCandidate);
                    try {
                        localJob = new LocalPseudoTcpJob(selectedPair.getDatagramSocket());
                    } catch (UnknownHostException ex) {
                        logger.log(Level.SEVERE, "Error while trying to create local pseudotcp thread " + ex);
                    }
                } else {
                    logger.log(Level.INFO, "Failed to select any candidate pair");
                }
            } else {
                if (iceProcessingState == IceProcessingState.TERMINATED || iceProcessingState == IceProcessingState.FAILED) {
                    /*
                     * Though the process will be instructed to die, demonstrate
                     * that Agent instances are to be explicitly prepared for
                     * garbage collection.
                     */
                    if (localJob != null && iceProcessingState == IceProcessingState.TERMINATED) {
                        System.out.println("start local job iceProcessingState = " + iceProcessingState);
                        localJob.start();
                    }
                    synchronized (localAgentMonitor) {
                        localAgentMonitor.notifyAll();
                    }
                }
            }
        }
    }

    private static class LocalPseudoTcpJob extends Thread implements Runnable {
        private DatagramSocket datagramSocket;

        LocalPseudoTcpJob(DatagramSocket socket) throws UnknownHostException {
            logger.log(Level.FINEST, "Local pseudotcp job created");

            this.datagramSocket = socket;
        }

        @Override
        public void run() {
            logger.log(Level.FINEST, "Local pseudotcp worker started");
            try {
                logger.log(Level.INFO, "Local pseudotcp is using: " + datagramSocket.getLocalSocketAddress() + datagramSocket);

                PseudoTcpSocket socket = new PseudoTcpSocketFactory().
                        createSocket(datagramSocket);
                socket.setConversationID(1073741824);
                socket.setMTU(1500);
                socket.setDebugName("L");
                socket.accept(5000);
                byte[] buffer = new byte[TEST_BYTES_COUNT];
                int read = 0;
                while (read != TEST_BYTES_COUNT) {
                    read += socket.getInputStream().read(buffer);
                    logger.log(Level.FINEST, "Local job read: " + read);
                }
                //TODO: close when all received data is acked
                //socket.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            logger.log(Level.FINEST, "Local pseudotcp worker finished");
        }
    }
}