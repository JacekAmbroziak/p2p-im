package controller;

import java.time.Instant;
import java.util.Date;
import java.util.Objects;

/**
 * User: jacek
 * Date: 4/3/20
 * Time: 9:03 AM
 * <p/>
 * Copyright 2020 Ambrosoft, Inc. All Rights Reserved.
 * This software is the proprietary information of Ambrosoft, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
final class SdpInfo {
    static final SdpInfo EMPTY = new SdpInfo("", new Date(0));

    final String sdpText;
    final Date created;
    final Instant instant;

    SdpInfo(final String sdpText, final Date created) {
        this.sdpText = Objects.requireNonNull(sdpText);
        this.created = Objects.requireNonNull(created);
        this.instant = created.toInstant();
    }

    boolean isEmpty() {
        return sdpText.isEmpty();
    }

    @Override
    public String toString() {
        return "SdpInfo{" +
                "sdp='" + sdpText + '\'' +
                ", created=" + created +
                '}';
    }
}
