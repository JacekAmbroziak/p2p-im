package controller;

import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created on 4/1/20
 */
public class Signalling {
    static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    static {
        SIMPLE_DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    //    static final String URL = "http://localhost:5000";
    static final String URL = "http://fast-bastion-38385.herokuapp.com";

    // insert into users (id,created,email) values ( default, now(), 'jacek.ambroziak@gmail.com' );
    //insert into sdp (id, created, user_id, value) values (DEFAULT, NOW(), (select id from users where email='marzena.trela@gmail.com'), 'xyzt');
    public static void main(String[] args) {

//        postSdp("marzena.trela@gmail.com", "I ryli like you!");
        try (final CloseableHttpClient httpclient = HttpClients.createDefault()) {
//            SdpInfo remoteSdp = getSdp(httpclient, "jacek.ambroziak@gmail.com");
            SdpInfo remoteSdp = getSdp(httpclient, "kwas");
            System.out.println("remoteSdp = " + remoteSdp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static void postSdp(final CloseableHttpClient httpclient, final String userEmail, final String sdpText) throws IOException {
        final HttpPost httpPost = new HttpPost(URL + "/sdp");
        {
            final List<NameValuePair> params = new ArrayList<>(2);
            params.add(new BasicNameValuePair("userEmail", userEmail));
            params.add(new BasicNameValuePair("sdpText", sdpText));
            httpPost.setEntity(new UrlEncodedFormEntity(params));
        }
        final ResponseHandler<String> responseHandler = response -> {
            final int status = response.getStatusLine().getStatusCode();
            if (status >= 200 && status < 300) {
                final HttpEntity entity = response.getEntity();
                return entity != null ? EntityUtils.toString(entity) : null;
            } else {
                throw new ClientProtocolException("Unexpected response status: " + status);
            }
        };

        final String responseBody = httpclient.execute(httpPost, responseHandler);
        System.out.println("----------------------------------------");
        System.out.println(responseBody);
    }

    static SdpInfo getSdp(final CloseableHttpClient httpclient, final String userEmail) throws ParseException, IOException {
        final HttpGet httpget = new HttpGet(URL + "/sdp?email=" + userEmail);
        final HttpResponse httpresponse = httpclient.execute(httpget);

        final Gson gson = new Gson();
        final Map map = gson.fromJson(EntityUtils.toString(httpresponse.getEntity()), Map.class);

        final List result = (List) map.get("result");
        if (!result.isEmpty()) {
            final Map map1 = (Map) result.get(0);
            return new SdpInfo((String) map1.get("sdp"), SIMPLE_DATE_FORMAT.parse((String) map1.get("created")));
        } else {
            return SdpInfo.EMPTY;
        }
    }

    public static String tradeSdpStringsWithRemote(final String mySdp, final String myEmail, final String remoteEmail) throws IOException, InterruptedException, ParseException {
        try (final CloseableHttpClient httpclient = HttpClients.createDefault()) {
            int attempts = 4;
            SdpInfo remoteSdp;
            do {
                final Instant startTime = Instant.now();
                // post ours
                postSdp(httpclient, myEmail, mySdp);
                // wait a bit
                Thread.sleep(3000);

                System.out.println("attempts = " + attempts + " startTime = " + startTime);

                // get remote sdp
                remoteSdp = getSdp(httpclient, remoteEmail);

                System.out.println("remoteSdp time = " + remoteSdp.instant);
                if (remoteSdp.instant.isAfter(startTime)) {
                    // very good: remote is even newer

                    break;
                } else if (Duration.between(remoteSdp.instant, startTime).compareTo(Duration.ofMinutes(1)) < 0) {
                    // remote is older but less than 1 minute: also good
                    break;
                } else {
                    // continue
                }
            } while (--attempts > 0);
            return remoteSdp.sdpText;
        }
    }
}
